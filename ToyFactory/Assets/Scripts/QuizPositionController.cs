using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class QuizPositionController : NetworkBehaviour
{
    public override void OnNetworkSpawn()
    {
        if (!IsOwner)
        {
            gameObject.SetActive(false);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        Debug.Log("���� " + other.gameObject);
        //Debug.Log(other.transform.parent.GetComponent<PlayerNetwork>());
        other.GetComponent<PlayerNetwork>().SetCanMove(false);
        GameObject.Find("QuizCanvas").transform.GetChild(0).gameObject.SetActive(true);
        this.GetComponent<NetworkObject>().gameObject.SetActive(false);
    }
}
