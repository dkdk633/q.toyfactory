using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Newtonsoft.Json;

[System.Serializable]
public class QuizWrapper
{
    public List<string> quiz;
}
public class ServerSelectQuizManager : MonoBehaviour
{
    private List<String> selectlist = new List<String>();


    // Start is called before the first frame update
    void Start()
    { 
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GetSelectQuiz()
    {
        selectlist = GameSceneUserDataManager.Instance().getQuizData();
        if (selectlist == null)
        {
            Debug.Log("null");
            
        }
        StartCoroutine(SendQuizToServer( selectlist ));
    }
    IEnumerator SendQuizToServer(List<string> quiz)
    {
        //string jsonData = JsonUtility.ToJson(new QuizWrapper { quiz = quiz });
        string jsonData = JsonConvert.SerializeObject(quiz); 
        Debug.Log(jsonData);
       
        UnityWebRequest request = new UnityWebRequest("http://localhost:1234/Selectquiz", "POST");
        byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(jsonData);
        request.uploadHandler = new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");

        yield return request.SendWebRequest();

        if (request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError)
        {
            Debug.LogError("http server error "+request.error);
        }
        else
        {
            Debug.Log("Quiz sent successfully!");
            //ClientReceiverManager.Instance.ClientGetQuizFromServer();
        }
    }
    
}
