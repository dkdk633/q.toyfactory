using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using Newtonsoft.Json;
using Unity.Netcode;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Networking;

public class ClientReceiverManager : NetworkBehaviour
{
    public static ClientReceiverManager Instance { get; private set; }

    public List<string> ClientQuestionlist;
    
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject); // 씬 전환 시 객체가 삭제되지 않도록 설정
        }
        else
        {
            Destroy(gameObject); // 중복 인스턴스가 있으면 삭제
        }
    }
    
    public override void OnNetworkSpawn()
    {
        // Do things with m_MeshRenderer
        if (!IsHost)
        {
            Debug.Log("client");
            ClientGetQuizFromServerRpc();
        }
        base.OnNetworkSpawn();
    }
    // Start is called before the first frame update
   
    
    [ServerRpc(RequireOwnership = false)]
    public void ClientGetQuizFromServerRpc()
    {
        Debug.Log("server");
        GetQuizClientRpc();
    }

    [ClientRpc]
    public void GetQuizClientRpc()
    {
        if (!IsHost)
        {
            Debug.Log("client");
            StartCoroutine(GetQuizFromServer());
        }
    }
    IEnumerator GetQuizFromServer()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://localhost:1234/Selectquiz");

        yield return request.SendWebRequest();

        if (request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError)
        {
            Debug.LogError(request.error);
        }
        else
        {
            string jsonData = request.downloadHandler.text;
            Debug.Log("Client "+ jsonData);
           // List<string> quiz = JsonUtility.FromJson<QuizWrapper>(jsonData).quiz;
           // JsonConvert를 사용하여 JSON 디시리얼라이즈
           List<string> quiz = JsonConvert.DeserializeObject<List<string>>(jsonData);
           ClientQuestionlist = quiz;
            GameSceneUserDataManager.Instance().setQuizData(quiz);
            //클라이언트가 server로 부터 받아옴
            Debug.Log("Client get "+quiz);
        }
    }

    public List<string> GetClientQuizData()
    {
        return ClientQuestionlist;
    }
}
