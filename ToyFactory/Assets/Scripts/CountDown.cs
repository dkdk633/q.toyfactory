using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Unity.Netcode;

public class CountDown : NetworkBehaviour
{
    private NetworkVariable<float> timer = new NetworkVariable<float>(6, NetworkVariableReadPermission.Everyone);

    [SerializeField] private TMP_Text timerText;

    private bool startCount = false;

    public override void OnNetworkSpawn()
    {
        timer.OnValueChanged += OnTimerChanged;
    }

    // Start is called before the first frame update
    void Start()
    {
        //timer.OnValueChanged += OnTimerChanged;
    }

    // Update is called once per frame
    void Update()
    {
        if (!IsHost)
            return;

        if (!startCount)
            return;

        if (timer.Value > 0)
        {
            timer.Value -= Time.deltaTime;
        }
        else 
        {
            int clientsNum = NetworkManager.Singleton.ConnectedClients.Count;
            for (ulong i = 0; i < (ulong)clientsNum; i++)
            {
                NetworkObject playerObject = NetworkManager.Singleton.ConnectedClients[i].PlayerObject;
                SetMoveClientRpc(playerObject.NetworkObjectId);
            }
            SetAcitveClientRpc();
        }
        
    }

    [ClientRpc]
    void SetMoveClientRpc(ulong id)
    {
        NetworkManager.Singleton.SpawnManager.SpawnedObjects[id].GetComponent<PlayerNetwork>().SetCanMove(true);
    }

    [ClientRpc]
    void SetAcitveClientRpc()
    {
        this.gameObject.SetActive(false);
    }

    private void OnTimerChanged(float oldValue, float newValue)
    {
        int newInt = (int)newValue;
        timerText.text = newInt.ToString();
    }

    public void SetStartCount(bool startCount)
    {
        this.startCount = startCount;
        timerText.text = timer.Value.ToString();
    }
}
