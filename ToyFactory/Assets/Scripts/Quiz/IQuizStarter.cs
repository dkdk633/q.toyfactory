﻿using System;

namespace Quiz
{
    public interface IQuizStarter
    {
        string GetUploadQuizData();

        void SetUploadQuizData(String quiz);
    }
}