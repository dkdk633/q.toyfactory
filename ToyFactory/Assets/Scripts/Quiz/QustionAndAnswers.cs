﻿using System;

namespace DefaultNamespace.Quiz
{
    [System.Serializable]
    public class QustionAndAnswers
    {
        public String Question;
        public String[] Answers;
        public int CorrectAnswer;
        public String Reply;
    }
}