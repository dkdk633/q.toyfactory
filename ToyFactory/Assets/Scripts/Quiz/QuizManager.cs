using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace.Quiz;
using iTextSharp.text.pdf.parser;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Random = UnityEngine.Random;
using Unity.Netcode;
using Newtonsoft.Json;


public class QuizManager : NetworkBehaviour
{
    public List<QustionAndAnswers> QnA;

    public QuizSystem quizSystem;
    private List<String> ql;
    void Start()
    {
        //QnA = new List<QustionAndAnswers>(); // QnA 리스트 초기화
        //AddQuestions();
        //quizSystem.SetQNA(QnA);
        //generateQuestion();
    }

    public override void OnNetworkSpawn()
    {
        //base.OnNetworkSpawn();
        QnA = new List<QustionAndAnswers>(); // QnA 리스트 초기화
        AddQuestions();
    }

    public void AddQuestions()
    {
        ql = GameSceneUserDataManager.Instance().getQuizData();

        if (ql == null){
            Debug.Log("0. add quiz null");
        }
        Debug.Log("1. add quiz");

        Debug.Log("check error");

        if (GameSceneUserDataManager.Instance().GetQuizOption() == 5)
        {
            Debug.Log("check error2");
            QuizOption_5(ql);
            Debug.Log("2. add quiz " + GameSceneUserDataManager.Instance().GetQuizOption());
        }
        else if (GameSceneUserDataManager.Instance().GetQuizOption() == 2)
        {
            QuizOption_2(ql);
            Debug.Log("2. add quiz "+ GameSceneUserDataManager.Instance().GetQuizOption());
        }

        quizSystem.SetQNA(QnA);
    }

    public void QuizOption_5(List<String> ql)
    {
        Debug.Log("5");
        List<string> questions = new List<string>();
        List<string> options = new List<string>();
        List<string> answers = new List<string>();
        List<string> replys = new List<string>();

        // 데이터 처리
        string currentQuestion = "";
        string currentOptions = "";

        foreach (string line in ql)
        {
            if (line.StartsWith("Q."))
            {
                // 질문일 경우
                currentQuestion = line;
            }
            else if (line.StartsWith("a)") || line.StartsWith("b)") || line.StartsWith("c)") || line.StartsWith("d)") ||
                     line.StartsWith("e)"))
            {
                // 정답 옵션일 경우
                currentOptions = line + "\n";
                options.Add(currentOptions);
            }
            else if (line.StartsWith("A."))
            {
                // 정답일 경우
                questions.Add(currentQuestion);
                answers.Add(line);
                currentOptions = ""; // 옵션 초기화
            }
            else if (line.StartsWith("R."))
            {
                line.Replace("R.",  " ");
                
                replys.Add(line);
            }
        }



        // 결과 출력
        for (int i = 0; i < questions.Count; i++)
        {
            Debug.Log($"질문: {questions[i]}");
            Debug.Log($"답변 옵션:\n{options[i]}");
            Debug.Log($"정답: {answers[i]}\n");
            Debug.Log($"해설: {replys[i]}\n");
            int ca = 0;
            if (answers[i].Contains("a"))
            {
                ca = 1;
            }
            else if (answers[i].Contains("b"))
            {
                ca = 2;

            }
            else if (answers[i].Contains("c"))
            {
                ca = 3;
            }
            else if (answers[i].Contains("d"))
            {
                ca = 4;
            }
            else if (answers[i].Contains("e"))
            {
                ca = 5;
            }

           
            QnA.Add(new QustionAndAnswers()
            {
                Question = questions[i],
                Answers = new string[]
                {
                    options[5 * i + 0], options[5 * i + 1], options[5 * i + 2], options[5 * i + 3],
                    options[5 * i + 4]
                },
                CorrectAnswer = ca,
                Reply = replys[i]
            });
        }


    }

    
    public void QuizOption_2(List<String> ql)
    {
        Debug.Log("2");
        List<string> questions = new List<string>();
        List<string> answers = new List<string>();
        List<string> replys = new List<string>();

        // 데이터 처리
        string currentQuestion = "";
        string currentOptions = "";

        foreach (string line in ql)
        {
            if (line.StartsWith("Q."))
            {
                // 질문일 경우
                currentQuestion = line;
            }
            else if (line.StartsWith("A."))
            {
                // 정답일 경우
                questions.Add(currentQuestion);
                answers.Add(line);
                currentOptions = ""; // 옵션 초기화
            }
            else if (line.StartsWith("R."))
            {
                line.Replace("R.",  " ");
                 
                replys.Add(line);
            }
        }



        // 결과 출력
        for (int i = 0; i < questions.Count; i++)
        {
            Debug.Log($"질문: {questions[i]}");
            Debug.Log($"정답: {answers[i]}\n");
            Debug.Log($"해설: {replys[i]}\n");
            int ca = 0;
            if (answers[i].Contains("O"))
            {
                ca = 1;
            }
            else if (answers[i].Contains("X"))
            {
                ca = 2;

            }

           
            QnA.Add(new QustionAndAnswers()
            {
                Question = questions[i],
                Answers = new string[]
                {
                    "O","X"
                },
                CorrectAnswer = ca,
                Reply = replys[i]
            });
            


        }
    }
    
    
    /*
    public void Correct()
    {
        generateQuestion();
    }


    void SetAnswer()
    {
        for (int i = 0; i < options.Length; i++)
        {
            options[i].GetComponent<AnswerScript>().isCorrect = false;
            options[i].transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = QnA[currentQuestion].Answers[i];

            if(QnA[currentQuestion].CorrectAnswer==i+1)
            {
                Debug.Log(QnA[currentQuestion].CorrectAnswer);
                options[i].GetComponent<AnswerScript>().isCorrect = true;
            }
        }
    }

    void generateQuestion()
    {

        Debug.Log(QnA.Count);

        // QnA 리스트가 비어 있는 경우
        if (QnA.Count == 0)
        {
            Debug.LogError("QnA 리스트가 비어 있습니다.");
            SceneLoader.Instance().LoadQuizScene("GameFin");
            return;
        }

        currentQuestion = Random.Range(0, QnA.Count);

        // 현재 질문이 리스트의 범위를 벗어나지 않도록 보정
        currentQuestion = Mathf.Clamp(currentQuestion, 0, QnA.Count - 1);

        QuestionTxt.text = QnA[currentQuestion].Question;
        SetAnswer();

        QnA.RemoveAt(currentQuestion);
    }
    */
    
}
