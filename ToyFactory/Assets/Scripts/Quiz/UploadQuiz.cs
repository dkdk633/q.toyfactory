using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DefaultNamespace;
using Quiz;
using TMPro;
using UnityEngine;
using UnityEngine.Networking; 
using UnityEngine.UI;
using Unity.Netcode;
using Org.BouncyCastle.Asn1;


[Serializable]
public class QuizEntityList
{
    public List<QuizEntity> quizzes;
}

[Serializable]
public class QuizEntity
{
    public int id;
    public String quizQues;
    public String lectureName;
    public String quizOption;
    public int executionCount;
}
public class UploadQuiz : NetworkBehaviour {

    public RectTransform contentPanel;
    public GameObject quizItemPrefab;
    
    private string serverURL = "http://localhost:1234/quiz";

    private quizGenerator _quizGenerator;

    private IQuizStarter iquiz;

    private int userid;

    private string json;

    private List<String> LecNameList;
    private List<String> QuizQuestionList;
    private List<int> OptionList;
    private List<int> quizidList;

    private String startquizQuestion;
    private int startquizInt;

    private List<string> Lists;
    
    public List<QuizItem> quizItems = new List<QuizItem>();

    private ServerSelectQuizManager _serverSelectQuizManager;
    // Start is called before the first frame update
    void Start()
    {
        _serverSelectQuizManager = FindObjectOfType<ServerSelectQuizManager>();
        LecNameList = new List<string>();
        userid = GameSceneUserDataManager.Instance().GetUserdata().id;
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void StartQuiz()
    {
        if (IsHost) {

            setQuizClientRpc(startquizQuestion, GameSceneUserDataManager.Instance().GetQuizOption());
        }

        // 버튼을 누른 후의 퀴즈 재 초기화s
        String quizQues = startquizQuestion;
        Debug.Log("quizQues: "+quizQues);
        
        // 초기화한 퀴즈 다듬기 :"-" 기준으로 split 하여 배열에 저장
        Lists = quizQues.Split(new[] { "\n","\r" }, StringSplitOptions.RemoveEmptyEntries).ToList();

        if (Lists.Count == 0)
        {
            Debug.Log("퀴즈 값이 이상하게 들어갔습니다.");
            return;
        }
        else
        {
            //server 일때
            GameSceneUserDataManager.Instance().setQuizData(Lists);
            Debug.Log("client set quiz successful");
            //Debug.Log("OPTION "+GameSceneUserDataManager.Instance().getQuizid());
            
            if (IsHost)
                StartCoroutine(wait());
            
        }
        
        //SceneLoader.Instance().LoadQuizScene("GamePlayScene_2");
    }

    [ClientRpc]
    public void setQuizClientRpc(string str, int num) {
        if (IsHost) return;
        startquizQuestion = str;
        GameSceneUserDataManager.Instance().SetQuizOption(num);
        StartQuiz();
    }

    IEnumerator wait() {
        yield return new WaitForSeconds(3);
        NetworkManager.Singleton.SceneManager.LoadScene("GamePlayScene_2", UnityEngine.SceneManagement.LoadSceneMode.Single);
    }
    
    private void HandleQuizButtonClicked(string quizData)
    {
        Debug.Log("Starting quiz: " + quizData);
        // 퀴즈 시작 로직 구현
        startquizQuestion = quizData;
    }
  

    public void Report()
    {
        StartCoroutine(PostRequest(serverURL, GameSceneUserDataManager.Instance().GetQuizString(), GameSceneUserDataManager.Instance().getFileName(),userid,GameSceneUserDataManager.Instance().GetQuizOption()));   
    }
    string responseText;

    // 서버에 POST 요청을 보내는 메서드
    IEnumerator PostRequest(string url, string quizQues,string LecName, int userId,int option)
    {
        // 요청 데이터 생성
        WWWForm form = new WWWForm();
        form.AddField("quizQues", quizQues);
        form.AddField("id", userId.ToString()); // 사용자 id 추가
        form.AddField("lectureName",LecName);
        form.AddField("quizOption",option.ToString());
        // POST 요청 보내기
        UnityWebRequest www = UnityWebRequest.Post(url, form);
        yield return www.SendWebRequest();
        
        // 요청 결과 확인
        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.LogError("Network error: " + www.error);
        }
        else
        {
            responseText = www.downloadHandler.text;
            Debug.Log("Quiz uploaded successfully");
        }
        

        if (responseText.Contains("success"))
        {
            Debug.Log("upload succeeded");
            
            
            Debug.Log(responseText);
            

        }
        else if(responseText.Contains("failed"))
        {
            Debug.Log("upload Failed: "+responseText);
         
        }
    }

    public void GetAllQuisList()
    {
        StartCoroutine(GetQuizzes(serverURL));
    }
    public void GetUserQuiz()
    {
        string serverURL_user = "http://localhost:1234/quiz/" +
                           GameSceneUserDataManager.Instance().GetUserdata().id;
        
        StartCoroutine(GetQuizzes(serverURL_user));
        
    }

    IEnumerator GetQuizzes(String url)
    {
        // 새로 초기화
        QuizQuestionList = new List<string>();
        LecNameList = new List<string>();
        OptionList = new List<int>();
        quizidList = new List<int>();
        UnityWebRequest www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.LogError("Failed to get quizzes: " + www.error);
        }
        else
        {
            // 서버로부터 받은 JSON 응답을 Quiz 배열로 파싱
            QuizEntityList quizEntities = JsonUtility.FromJson<QuizEntityList>("{\"quizzes\":" + www.downloadHandler.text + "}");
            
            // 받은 퀴즈 리스트를 처리 (예: 각 퀴즈별로 로그로 출력)
            foreach (var quiz in quizEntities.quizzes)
            {
                QuizQuestionList.Add(quiz.quizQues);
                LecNameList.Add(quiz.lectureName);
                OptionList.Add(int.Parse(quiz.quizOption));
                quizidList.Add(quiz.id);
            }
            PopulateQuiz();
        }
    }
    
    public void PopulateQuiz()
    {
        quizItems.Clear();
        RemoveAllChildren();
       
        Debug.Log("Open"+LecNameList.Count);
        for (int i = 0; i < LecNameList.Count; i++)
        {
            GameObject newItem = Instantiate(quizItemPrefab, contentPanel);
            QuizItem quizItem = newItem.GetComponent<QuizItem>();
            quizItem.Setup(LecNameList[i],QuizQuestionList[i],OptionList[i],quizidList[i]);
            quizItem.OnQuizButtonClick += HandleQuizButtonClicked;
            
            quizItems.Add(quizItem);
        }
        Debug.Log("quiz count"+quizItems.Count);
    }

    public List<String> GetUserQuizData()
    {
        return LecNameList;
    }
    void RemoveAllChildren()
    {
        // content 내부의 모든 자식을 제거
        foreach (Transform child in contentPanel)
        {
            Destroy(child.gameObject);
        }
    }

    public void setQuizStr(string str) {
        startquizQuestion = str;
    }
   
}
    
    
    

