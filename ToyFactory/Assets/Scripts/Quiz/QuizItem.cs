﻿using System;
using Quiz;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class QuizItem : MonoBehaviour , IQuizStarter
{
    public TMP_Text questionText;

    public Button button;

    [SerializeField] private String Question;

    [SerializeField] private int option;
    
    [SerializeField] private int id;
    
    public delegate void QuizItemEventHandler(string quizData);
    public event QuizItemEventHandler OnQuizButtonClick;
    
    // 생성자 생성
    public void Setup(String btntext,String question,int option,int id)
    {
        questionText.text = btntext;
        Question = question;
        this.option = option;
        this.id = id;
        
        button.onClick.AddListener(HandleQuizButtonClick);
    }
    private void HandleQuizButtonClick()
    {
        if (OnQuizButtonClick != null)
        {
            OnQuizButtonClick(Question);
            Debug.Log("put in the option to gameSceneuser"+option);
            GameSceneUserDataManager.Instance().SetQuizOption(option);
            GameSceneUserDataManager.Instance().setQuizid(id);
        }
    }
    
    private void ExecuteQuiz()
    {
        // 여기에서 퀴즈 실행 로직을 구현
    }

    public String BtnQuizQuestion()
    {
        return Question;
    }
    

    public string GetUploadQuizData()
    {
        return Question;
    }

    public void SetUploadQuizData(String quiz)
    {
        Question = quiz;
    }

    public int GetOption()
    {
        return option;
    }
}