using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Unity.Netcode;

public class QuizUpdate : NetworkBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
            StartCoroutine(UpdateQuizExecutionCount(GameSceneUserDataManager.Instance().getQuizid()));

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator UpdateQuizExecutionCount(int quizId)
    {
     
        string url = $"http://localhost:1234/quiz-execution/{quizId}/increment";
        UnityWebRequest request = UnityWebRequest.PostWwwForm(url, "");

        yield return request.SendWebRequest();
        Debug.Log("url " + url);
        if (request.result != UnityWebRequest.Result.ConnectionError && request.result != UnityWebRequest.Result.ProtocolError)
        {
            Debug.Log("Execution count updated successfully");
        }
        else
        {
            Debug.LogError("Failed to update execution count: " + request.error);
        }
    }
}
