using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;


[System.Serializable]
public class UserData
{
    public int id;
    public string userlogin;
    public string username;
    public string password;
}
public class GameSceneUserDataManager: MonoBehaviour
{
    
    private static GameSceneUserDataManager instance=new GameSceneUserDataManager();

    private UserData userData;

    public List<String> QuestionData;
    
    public String QuizData;

    private List<String> userQuizList;
    private string fileName;

    private int option;
    
    public int quizid;

    private GameSceneUserDataManager() {
        // 생성자는 외부에서 호출못하게 private 으로 지정해야 한다.
    }

    public static GameSceneUserDataManager Instance() {
        return instance;
    }

    void Awake()
    {
        if (instance == null)
        {
            // 씬 전환 시 파괴되지 않도록 설정
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else
        {
            // 이미 인스턴스가 존재하면 이전 인스턴스를 파괴
            Destroy(gameObject);
        }
    }
    
    private string json;
   // public GameObject id;
    //public GameObject username;

    public void setJsonData(String json)
    {
        userData = JsonUtility.FromJson<UserData>(json);
        
        Debug.Log(userData);
        
        Debug.Log("ID: " + userData.id);
        Debug.Log("loginid"+userData.userlogin);
        Debug.Log("Username: " + userData.username);
        Debug.Log("Password: " + userData.password);

       
        
    }

    public String GetUserName()
    {
        Debug.Log(userData.username);
        return userData.username;
    }

    public void SetUserQuizList(List<String> list)
    {
        userQuizList = list;
    }

    public List<String> GetUserQuizList()
    {
        return userQuizList;
    }

    public UserData GetUserdata()
    {
        return userData;
    }

    public void setQuizData(List<String> str)
    {
        
        
        QuestionData = str;
        

    }

    public void SetQuizString(String str)
    {
        QuizData = str;
    }

    public String GetQuizString()
    {
        //Debug.Log("quiz : "+QuizData);
        return QuizData;
    }

    public List<String> getQuizData()
    {
        return QuestionData;
    }

    public int GetQuizOption()
    {
        return option;
    }

    public void SetQuizOption(int s)
    {
        option = s;
    }
    public void setQuizid(int id)
    {
        quizid = id;
    }

    public int getQuizid()
    {
        return quizid;
    }

    public void setFileName(string name) { 
        fileName = name;
    }

    public string getFileName() {
        return fileName;
    }
}
