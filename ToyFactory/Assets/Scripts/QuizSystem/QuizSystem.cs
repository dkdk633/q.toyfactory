﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DefaultNamespace.Quiz;
using TMPro;
using UnityEngine.UI;
using Unity.Netcode;
using System;
using UnityEngine.SceneManagement;

public class QuizSystem : NetworkBehaviour
{

    private List<Transform> quizes = new List<Transform>();

    [SerializeField]
    private GameObject gaugeBar;

    public double gaugeIncrease = 0;
    private int panelNum = 0;
    private AudioSource errorSound;
    [SerializeField]
    private GameObject conveyor;
    private bool isLastQuiz = false;

    private int quiztype=5;
    private List<QustionAndAnswers> qna;

    [SerializeField] GameObject multiplePrefab;
    [SerializeField] GameObject oxPrefab;

    [SerializeField]
    private int targetToyNum;

    // is~Finish) -1 : not finish yet, 0 : fail, 1 : success 
    public NetworkVariable<int> isHostFinish = new NetworkVariable<int>(-1, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner);
    public NetworkVariable<int> isClientFinish = new NetworkVariable<int>(-1, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner);

    [SerializeField]
    private GameObject gameResultPanel;

    // Start is called before the first frame update
    void Start()
    {
        //Transform[] allQuiz = GetComponentsInChildren<Transform>();
        for (int i = 0; i < transform.childCount; i++)
        {
            quizes.Add(transform.GetChild(i));
            Debug.Log(quizes);
        }
        Debug.Log("Quizes.count: " + quizes.Count);
        errorSound = GetComponent<AudioSource>();

        if (NetworkManager.Singleton != null)
        {
            Debug.Log("NetworkManager is initialized");
            if (NetworkManager.Singleton.IsHost)
            {
                Debug.Log("This instance is the host");
            }
        }
        else
        {
            Debug.LogWarning("NetworkManager is not initialized");
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SelectCorrectAnswer()
    {
        Debug.Log("correct!");
        gaugeBar.GetComponent<GaugeBar>().IncreaseGauge(gaugeIncrease);
        //NextQuiz();
    }

    public void SelectWrongAnswer()
    {
        Debug.Log("wrong!");
        //sound
        errorSound.Play();
        //NextQuiz();
    }

    public void NextQuiz()
    {
        //quizes[panelNum].gameObject.SetActive(false);
        Destroy(quizes[panelNum].gameObject);
        Debug.Log("Current Panel: " + panelNum);
        
        if (++panelNum <= quizes.Count/2 - 1)
        {
            quizes[panelNum].gameObject.SetActive(true);
        }
        else //������ ��� Ǯ���� ��
        {
            isLastQuiz = true;
            Debug.Log(isLastQuiz);
            if (gaugeBar.GetComponent<GaugeBar>().totalValue == targetToyNum)
            {
                StartCoroutine(GameResult(true));
            }
            else 
            {
                StartCoroutine(GameResult(false));
            }
        }
    }
    private IEnumerator GameResult(bool win)
    {
        yield return new WaitForSeconds(5f);
        if (IsHost)
        {
            if (NetworkManager.Singleton.ConnectedClients.Count < 2)
            {
                if (win)
                {
                    isHostFinish.Value = 1;
                    gameResultPanel.transform.GetChild(0).GetChild(0).GetComponent<TMP_Text>().text = "탈출 성공!";
                    gameResultPanel.SetActive(true);
                    Invoke("Return", 3f);
                }
                else 
                {
                    isHostFinish.Value = 0;
                    gameResultPanel.transform.GetChild(0).GetChild(0).GetComponent<TMP_Text>().text = "탈출 실패!";
                    gameResultPanel.SetActive(true);
                    Invoke("Return", 3f);
                }
            }
            else 
            {
                if(win) // 이겼으면 상관없이 상대도 lose 뜨게 하기!
                {
                    isHostFinish.Value = 1;
                    gameResultPanel.transform.GetChild(0).GetChild(0).GetComponent<TMP_Text>().text = "탈출 성공!";
                    gameResultPanel.SetActive(true);
                    //Debug.Log("Win!");
                    HostWinClientRpc();
                }
                else // 상대가 이미 진 상태면 게임 끝내기.
                {
                    gameResultPanel.transform.GetChild(0).GetChild(0).GetComponent<TMP_Text>().text = "탈출 실패!";
                    gameResultPanel.SetActive(true);
                    //Debug.Log("Lose!");
                    isHostFinish.Value = 0;
                    if (isClientFinish.Value == 0)
                    {
                        ReturnClientRpc();
                    }
                }
            }  
        }
        else 
        {
            if (win) // 이겼으면 상관없이 상대도 lose 뜨게 하기!
            {
                UpdateIsClientFinishServerRpc(1);
                gameResultPanel.transform.GetChild(0).GetChild(0).GetComponent<TMP_Text>().text = "탈출 성공!";
                gameResultPanel.SetActive(true);
                //Debug.Log("Win!");
                ClientWinServerRpc();
            }
            else // 상대가 이미 진 상태면 게임 끝내기.
            {
                gameResultPanel.transform.GetChild(0).GetChild(0).GetComponent<TMP_Text>().text = "탈출 실패!";
                gameResultPanel.SetActive(true);
                //Debug.Log("Lose!");
                UpdateIsClientFinishServerRpc(0);
                if (isHostFinish.Value == 0)
                {
                    ReturnServerRpc();
                }
            }
        }

    }

    [ServerRpc(RequireOwnership = false)]
    public void UpdateIsClientFinishServerRpc(int state)
    {
        isClientFinish.Value = state;
    }

    [ClientRpc]
    public void HostWinClientRpc()
    {
        if (!IsHost) 
        {
            Debug.Log("Lose!");
        }

        ReturnClientRpc();
    }

    [ServerRpc(RequireOwnership = false)]
    public void ClientWinServerRpc()
    {
        if (IsHost)
        {
            Debug.Log("Lose!");
        }

        ReturnClientRpc();
    }

    [ServerRpc(RequireOwnership = false)]
    public void ReturnServerRpc()
    {
        ReturnClientRpc();
    }

    [ClientRpc]
    public void ReturnClientRpc()
    {
        Invoke("Return", 3f);
    }


    public void Return()
    {
        NetworkManager.Shutdown();
        
    }

    public override void OnNetworkDespawn()
    {
        SceneManager.LoadScene("Factory_asset", LoadSceneMode.Single);
    }

    public void MakeQuiz()
    {
        //�ڵ����� quiz �����
        if (qna == null)
            return;

        //if (!NetworkManager.Singleton.IsHost)
        //    return;

        Debug.Log("MakeQuiz!");

        //foreach (ulong id in NetworkManager.ConnectedClientsIds)
        //{
            for (int i = 0; i < qna.Count; i++)
            {
                GameObject newQuiz;
                if (GameSceneUserDataManager.Instance().GetQuizOption() == 5)
                {
                    newQuiz = Instantiate(multiplePrefab, transform);
                }
                else if (GameSceneUserDataManager.Instance().GetQuizOption() == 2)
                {
                    newQuiz = Instantiate(oxPrefab, transform);
                }
                else
                {
                    newQuiz = Instantiate(multiplePrefab, transform);
                }

                newQuiz.transform.GetChild(0).GetComponent<TMP_Text>().text = qna[i].Question;
                newQuiz.transform.GetChild(2).GetChild(0).GetComponent<TMP_Text>().text = qna[i].Reply;
                newQuiz.transform.GetChild(2).GetChild(1).GetComponent<Button>().onClick.AddListener(NextQuiz);

                for (int j = 0; j < qna[i].Answers.Length; j++)
                {
                    Transform currentOption = newQuiz.transform.GetChild(1).GetChild(j);

                    currentOption.GetChild(0).GetComponent<TMP_Text>().text = qna[i].Answers[j];
                    if (qna[i].CorrectAnswer == j + 1)
                    {
                        //���̸� correctAnswer �޾��ֱ�
                        currentOption.GetComponent<Button>().onClick.AddListener(SelectCorrectAnswer);
                    }
                    else
                    {
                        //���̸� wrongAnswer �޾��ֱ�
                        currentOption.GetComponent<Button>().onClick.AddListener(SelectWrongAnswer);
                    }
                }

                quizes.Add(newQuiz.transform);
                newQuiz.SetActive(false);

                //NetworkObject quiz = newQuiz.GetComponent<NetworkObject>();
                //quiz.SpawnWithOwnership(id, true);
                //newQuiz.transform.SetParent(transform, false);
            }
        //}
    }


    public int GetTargetToyNum()
    {
        return targetToyNum;
    }

    public bool GetIsLastQuiz()
    {
        return isLastQuiz;
    }

    public void SetQNA(List<QustionAndAnswers> qna)
    {
        this.qna = qna;
        MakeQuiz();
        gaugeIncrease = Math.Ceiling(((double)targetToyNum / qna.Count) * 100f) / 100f;
    }

    public void SetConveyor(GameObject conveyor)
    {
        this.conveyor = conveyor;
    }

    public int SetQuizType()
    {
        return GameSceneUserDataManager.Instance().GetQuizOption();
    }

    public void GetQuizType(int quiz)
    {
        quiztype = quiz;
    }
    public void SetGaugeBar(GameObject gaugeBar)
    {
        this.gaugeBar = gaugeBar;
    }
}
