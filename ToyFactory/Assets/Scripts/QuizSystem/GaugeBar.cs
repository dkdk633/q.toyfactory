using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity.Netcode;

public class GaugeBar : NetworkBehaviour
{
    private Slider slider;
    public List<GameObject> toyPrefabs;
    public Transform spawnTransform;
    public double targetValue;
    public double totalValue;
    public NetworkVariable<double> hostTotalGauge = new NetworkVariable<double>(0, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner);
    public NetworkVariable<double> clientTotalGauge = new NetworkVariable<double>(0, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner);
    [SerializeField] private Slider mySlider;
    [SerializeField] private Slider opponentSlider;

    public float fillSpeed;
    public GameObject conveyor;
    private int toyPrefabNum;
    private GameObject currentToy;
    public override void OnNetworkSpawn()
    {
        hostTotalGauge.OnValueChanged += OnHostGaugeChanged;
        clientTotalGauge.OnValueChanged += OnClientGaugeChanged;
        mySlider = GameObject.Find("ToyCanvas").transform.GetChild(2).GetComponent<Slider>();
        opponentSlider = GameObject.Find("ToyCanvas").transform.GetChild(3).GetComponent<Slider>();

        if (IsHost)
        {
            if (NetworkManager.Singleton.ConnectedClients.Count == 1)
            {
                mySlider.gameObject.SetActive(true);
                GameObject.Find("ToyCanvas").transform.GetChild(4).gameObject.SetActive(true);
            }
            else
            {
                mySlider.gameObject.SetActive(true);
                GameObject.Find("ToyCanvas").transform.GetChild(4).gameObject.SetActive(true);
                opponentSlider.gameObject.SetActive(true);
                GameObject.Find("ToyCanvas").transform.GetChild(5).gameObject.SetActive(true);
            }
        }
        else
        {
            mySlider.gameObject.SetActive(true);
            GameObject.Find("ToyCanvas").transform.GetChild(4).gameObject.SetActive(true);
            opponentSlider.gameObject.SetActive(true);
            GameObject.Find("ToyCanvas").transform.GetChild(5).gameObject.SetActive(true);
        }
        slider = GetComponent<Slider>();
        slider.value = 0f;

        if (!IsOwner)
        {
            transform.parent.GetComponent<Canvas>().enabled = false;
        }
        toyPrefabNum = toyPrefabs.Count;
    }

    private void OnHostGaugeChanged(double oldValue, double newValue)
    {
        double targetValue = newValue / 3; //장난감 수로 나누기
        Debug.Log("host target value: " + targetValue);
        if (IsHost)
        {
            Debug.Log("host increase their gauge : " + targetValue);
            StartCoroutine(FillMyGauge(targetValue));
        }
        else 
        {
            Debug.Log("host increase opponent gauge : " + targetValue);
            StartCoroutine(FillOpponentGauge(targetValue));
        }
    }

    private void OnClientGaugeChanged(double oldValue, double newValue)
    {
        double targetValue = newValue / 3; //장난감 수로 나누기
        Debug.Log("client target value: " + targetValue);
        if (!IsHost)
        {
            Debug.Log("client increase their gauge : " + targetValue);
            StartCoroutine(FillMyGauge(targetValue));
        }
        else
        {
            Debug.Log("client increase opponent gauge : " + targetValue);
            StartCoroutine(FillOpponentGauge(targetValue));
        }
    }

    public void Update()
    {
        if (!IsOwner)
            return;
       
        if (Mathf.Approximately(slider.value, 1f))
        {
            targetValue = 0f;
            slider.value = 0f;
            MakeToy();
        }

    }

    public void MakeToy()
    {
        if (!IsOwner)
            return;
        int i = Random.Range(0, toyPrefabNum);
        //GameObject newToy = Instantiate(toyPrefabs[i], spawnTransform.position, spawnTransform.rotation);
        //
        SpawnToyServerRpc(i);
        conveyor.GetComponent<Conveyor>().SetCurrentToy(currentToy);
        Debug.Log("currentToy: " + currentToy);
    }

    [ServerRpc(RequireOwnership = false)]
    public void SpawnToyServerRpc(int toyIndex, ServerRpcParams serverRpcParams = default)
    {
        ulong clientId = serverRpcParams.Receive.SenderClientId;
        GameObject newToy = Instantiate(NetworkManager.GetNetworkPrefabOverride(toyPrefabs[toyIndex]), spawnTransform.position, spawnTransform.rotation);
        newToy.GetComponent<NetworkObject>().SpawnWithOwnership(clientId, true);
        SetCurrentToyClientRpc(newToy);
    }

    [ClientRpc]
    public void SetCurrentToyClientRpc(NetworkObjectReference toyObject)
    {
        if (!toyObject.TryGet(out NetworkObject networkObject))
        {
            Debug.Log("can't find networkObject of toy");
        }
        currentToy = networkObject.gameObject;
    }

    IEnumerator FillGauge()
    {
        while (slider.value < targetValue)
        {
            slider.value += fillSpeed * Time.deltaTime;
            yield return null;
        }
    }

    IEnumerator FillMyGauge(double target)
    {
        while (mySlider.value < target)
        {
            mySlider.value += fillSpeed * Time.deltaTime;
            yield return null;
        }
    }

    IEnumerator FillOpponentGauge(double target)
    {
        while (opponentSlider.value < target)
        {
            opponentSlider.value += fillSpeed * Time.deltaTime;
            yield return null;
        }
    }

    public void IncreaseGauge(double gaugeIncrease)
    {
        double newValue = slider.value + gaugeIncrease;
        totalValue += gaugeIncrease;
        if (IsHost)
        {
            Debug.Log("host increase gauge");
            hostTotalGauge.Value = totalValue;
        }
        else 
        {
            Debug.Log("client increase gauge");
            clientTotalGauge.Value = totalValue;
        }

        if (newValue > 1.0)
        {
            targetValue = 1.0; // 최대 값인 1로 설정
            double excess1 = newValue - 1.0; // 초과된 값 계산
            double excess2 = totalValue - (int)totalValue;
            StartCoroutine(FillGauge());
            StartCoroutine(FillRemaining(excess2)); // 초과된 값만큼 다시 채우는 코루틴 실행
        }
        else
        {
            targetValue = newValue;
            StartCoroutine(FillGauge());
        }
        Debug.Log("host total gauge: " + hostTotalGauge.Value);
        Debug.Log("client total gauge: " + clientTotalGauge.Value);
    }

    private IEnumerator FillRemaining(double excess)
    {
        yield return new WaitUntil(() => slider.value == 1.0f);
        MakeToy();
        slider.value = 0.0f; // 슬라이더를 0으로 재설정
        targetValue = excess; // 초과된 값부터 다시 채워지도록
        StartCoroutine(FillGauge());
    }

}