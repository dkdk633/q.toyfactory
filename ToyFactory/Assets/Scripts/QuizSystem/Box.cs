using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour
{
    [SerializeField] private Conveyor conveyor;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Toy")
        {
            conveyor.GetToy(other.gameObject);
        }

    }
}
