using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;
using Unity.Services.Relay;
using Unity.Services.Lobbies;
using Unity.Services.Relay.Models;
using Unity.Services.Core;
using Unity.Services.Authentication;
using Unity.Netcode.Transports.UTP;
using Unity.Networking.Transport.Relay;
using Unity.Services.Lobbies.Models;
using UnityEngine.UI;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using WebSocketSharp;
using TMPro;
using Newtonsoft.Json.Linq;
using UnityEngine.SceneManagement;

public class RelayLobby : NetworkBehaviour
{
    private List<Lobby> lobbyList;
    [SerializeField] GameObject lobbyButton, contentView, LobbyCreationPanel, InLobbyPenel, LobbyListPenel, gameStartButton, lobbyPWUI, clientLobbyPWUI, quizListButton, quizWaitText;
    [SerializeField] GameObject clientWaitingImage, clientInformObj;
    [SerializeField] TextMeshProUGUI hostPlayerId, clientPlayerId;
    [SerializeField] Toggle lobbyToggle, readyToggle;
    [SerializeField] TMP_InputField lobbyNameInput, passwordInput, clientPasswordInput;
    private string myLobbyId;
    private bool isReady = false;
    private Lobby lobbyTryToEnter;
    public string userId="User Name";
    string playerId;
    [SerializeField] UploadQuiz _uploadQuiz;

    void Start()
    {
        userId = GameSceneUserDataManager.Instance().GetUserName();
        quizWaitText.SetActive(false);
        lobbyPWUI.SetActive(false);
        clientLobbyPWUI.SetActive(false);
        lobbyToggle.onValueChanged.AddListener((bool isPrivate) => {
            if (isPrivate)
            {
                lobbyPWUI.SetActive(true);
            }
            else
            {
                lobbyPWUI.SetActive(false);
            }
        });
        readyToggle.onValueChanged.AddListener((bool isOn) =>
        {
            if (IsHost)
            {
                if (isOn)
                {
                    if (clientPlayerId.text.IsNullOrEmpty()) isReady = true;
                    //if (isReady) NetworkManager.Singleton.SceneManager.LoadScene("factory_asset_hy", UnityEngine.SceneManagement.LoadSceneMode.Single);
                    if (isReady) _uploadQuiz.StartQuiz();//NetworkManager.Singleton.SceneManager.LoadScene("GamePlayScene_2", UnityEngine.SceneManagement.LoadSceneMode.Single);
                    else
                    {
                        Debug.Log("client not ready");
                        readyToggle.isOn = false;
                    }
                }
            }
            else
            {

                if (isOn)
                {
                    readyServerRpc(true);
                }
                else
                {
                    readyServerRpc(false);
                }
            }
        });
        init();
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        if (IsHost)
        {
            LobbyCreationPanel.SetActive(false);
            InLobbyPenel.SetActive(true);
            clientWaitingImage.SetActive(true);
            clientInformObj.SetActive(false);
            lobbyNameInput.text = "";
            passwordInput.text = "";
            lobbyToggle.isOn = false;
            quizListButton.GetComponent<Button>().interactable = true;
        }
        else
        {
            updateClientInformServerRpc(userId);
            LobbyListPenel.SetActive(false);
            InLobbyPenel.SetActive(true);
            quizListButton.GetComponent<Button>().interactable = false;
        }
    }

    [ServerRpc(RequireOwnership = false)]
    public void readyServerRpc(bool ready)
    {
        isReady = ready;
    }

    private async void init()
    {
        await UnityServices.InitializeAsync();
        if (!AuthenticationService.Instance.IsSignedIn)
        {
            AuthenticationService.Instance.ClearSessionToken();
            await AuthenticationService.Instance.SignInAnonymouslyAsync();
        }
        //else { 
        //    NetworkManager.Shutdown();
        //}

        playerId = AuthenticationService.Instance.PlayerId;
        Debug.Log(AuthenticationService.Instance.PlayerId);
    }


    public async void relayAllocation()
    {
        if (lobbyNameInput.text.IsNullOrEmpty()) return;
        if (lobbyToggle.isOn)
        {
            if (passwordInput.text.IsNullOrEmpty()) return;
            if (passwordInput.text.Length < 8 || passwordInput.text.Length > 64) return;
        }

        Allocation allocatoin = await Unity.Services.Relay.RelayService.Instance.CreateAllocationAsync(2);
        string joinCode = await Unity.Services.Relay.RelayService.Instance.GetJoinCodeAsync(allocatoin.AllocationId);
        NetworkManager.Singleton.GetComponent<UnityTransport>().SetRelayServerData(new RelayServerData(allocatoin, "dtls"));

        createLobby(joinCode);
    }

    private async void createLobby(string joincode)
    {
        isReady = false;
        string lobbyName = lobbyNameInput.text;
        int maxPlayer = 2;

        CreateLobbyOptions options = new CreateLobbyOptions();

        if (lobbyToggle.isOn)
        {
            options.IsPrivate = false;
            options.Password = passwordInput.text; //8~64�ڿ�����
        }
        else
        {
            options.IsPrivate = false;
        }

        options.Data = new Dictionary<string, DataObject>() {
            {
                "relayJoinCode", new DataObject(
                        visibility: DataObject.VisibilityOptions.Public,
                        value: joincode
                    )
            }
        };

        Lobby lobby = await LobbyService.Instance.CreateLobbyAsync(lobbyName, maxPlayer, options);
        myLobbyId = lobby.Id;
        isReady = false;
        StartCoroutine(HeartbeatLobbyCoroutine(lobby.Id, 15));
        Debug.Log(lobby.LobbyCode);

        hostPlayerId.text = userId;
        clientWaitingImage.SetActive(true);
        gameStartButton.transform.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>().text = "게임 시작";
        gameStartButton.GetComponent<Toggle>().interactable = false;
        NetworkManager.Singleton.StartHost();
    }

    public async void joinLobby(string joinCode, Lobby lobby)
    {
        try
        {
            if (lobby.HasPassword)
            {
                var idOptions = new JoinLobbyByIdOptions { Password = clientPasswordInput.text };
                await LobbyService.Instance.JoinLobbyByIdAsync(lobby.Id, idOptions);
            }
            else
            {
                await LobbyService.Instance.JoinLobbyByIdAsync(lobby.Id);
            }

            JoinAllocation allocation = await Unity.Services.Relay.Relay.Instance.JoinAllocationAsync(joinCode);
            NetworkManager.Singleton.GetComponent<UnityTransport>().SetRelayServerData(new RelayServerData(allocation, "dtls"));

            myLobbyId = lobby.Id;
            gameStartButton.GetComponent<Toggle>().interactable = true;
            gameStartButton.transform.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>().text = "Ready";
            NetworkManager.Singleton.StartClient();
        }
        catch (LobbyServiceException e)
        {
            Debug.Log(e);
        }
    }

    public async void refreshLobbyList()
    {
        foreach (Transform child in contentView.transform)
        {
            Destroy(child.gameObject);
        }

        try
        {
            QueryLobbiesOptions options = new QueryLobbiesOptions();
            options.Count = 25;

            options.Filters = new List<QueryFilter>() {
            new QueryFilter(
                field: QueryFilter.FieldOptions.AvailableSlots,
                op: QueryFilter.OpOptions.GT,
                value: "0"
                )
        };

            options.Order = new List<QueryOrder>() {
            new QueryOrder(
                asc: false,
                field: QueryOrder.FieldOptions.Created
                )
        };
            QueryResponse lobbies = await Lobbies.Instance.QueryLobbiesAsync(options);
            lobbyList = lobbies.Results;

            int tempNum = 0;

            if (lobbyList.Count == 0) Debug.Log("�����ϴ� �κ� �����ϴ�.");

            for (int i = 0; i < lobbyList.Count; i++)
            {
                tempNum = i;
                GameObject newBtn = Instantiate(lobbyButton);
                if (SceneManager.GetActiveScene().name.Equals("factory_asset")) {
                    newBtn.transform.SetParent(contentView.transform);
                    newBtn.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = lobbyList[tempNum].Name;
                    newBtn.GetComponent<Button>().onClick.AddListener(() => {
                        if (lobbyList[tempNum].HasPassword)
                        {
                            clientLobbyPWUI.SetActive(true);
                            lobbyTryToEnter = lobbyList[tempNum];
                        }
                        else
                        {
                            joinLobby(lobbyList[tempNum].Data["relayJoinCode"].Value, lobbyList[tempNum]);
                        }
                    });
                }
            }

        }
        catch (LobbyServiceException e)
        {
            Debug.Log(e);
        }
    }

    public void checkPassword()
    {
        joinLobby(lobbyTryToEnter.Data["relayJoinCode"].Value, lobbyTryToEnter);
        clientPasswordInput.text = "";
        clientLobbyPWUI.SetActive(false);
        lobbyTryToEnter = null;
    }

    public async void leaveLobby()
    {
        try
        {
            if (NetworkManager.Singleton.IsHost)
            {
                //await LobbyService.Instance.RemovePlayerAsync(myLobbyId, playerId);
                await LobbyService.Instance.DeleteLobbyAsync(myLobbyId);
            }
            else
            {
                //var lobbyId = await LobbyService.Instance.GetJoinedLobbiesAsync();
                clientPlayerId.text = "";
                showWaitingTextServerRpc();
                await LobbyService.Instance.RemovePlayerAsync(myLobbyId, playerId);
            }
            NetworkManager.Singleton.Shutdown();
        }
        catch (LobbyServiceException e)
        {
            Debug.Log(e);
        }
    }

    [ServerRpc(RequireOwnership = false)]
    public void showWaitingTextServerRpc() {
        clientWaitingImage.SetActive(true);
        clientInformObj.SetActive(false);
    }

    public override void OnNetworkDespawn()
    {
        base.OnNetworkDespawn();
        refreshLobbyList();
        InLobbyPenel.SetActive(false);
        LobbyListPenel.SetActive(true);
        myLobbyId = "";
    }

    IEnumerator HeartbeatLobbyCoroutine(string lobbyId, float waitTimeSeconds)
    {

        var delay = new WaitForSeconds(waitTimeSeconds);

        while (true)
        {
            LobbyService.Instance.SendHeartbeatPingAsync(lobbyId);
            yield return delay;
        }
    }

    //ConcurrentQueue<string> createdLobbyIds = new ConcurrentQueue<string>();

    private void OnApplicationQuit()
    {
        if (!myLobbyId.IsNullOrEmpty() /*&& NetworkManager.Singleton.IsHost*/)
        {
            Debug.Log("�κ� ���� ��");
            LobbyService.Instance.DeleteLobbyAsync(myLobbyId);
            myLobbyId = "";
        }
    }

    public void closePanel()
    {
        clientLobbyPWUI.SetActive(false);
        lobbyTryToEnter = null;
    }

    [ServerRpc(RequireOwnership = false)]
    public void updateClientInformServerRpc(string clientUserID)
    {
        updateClientInformClientRpc(clientUserID, userId);
    }

    [ClientRpc]
    public void updateClientInformClientRpc(string userClientId, string userHostId)
    {
        clientPlayerId.text = userClientId;
        hostPlayerId.text = userHostId;
        clientWaitingImage.SetActive(false);
        clientInformObj.SetActive(true);
    }
}
