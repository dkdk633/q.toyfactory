using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class loadLobbyScene : MonoBehaviour
{
    public void startLobby() {
        SceneManager.LoadScene("FactoryScene");
    }

 
    public void GoRanking()
    {
        SceneManager.LoadScene("RankingScene");
    }
}
