using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using TMPro;

public class CharacterSpawner : NetworkBehaviour
{
    [SerializeField]
    private GameObject player;
    [SerializeField]
    private GameObject machine;
    [SerializeField]
    private GameObject effect;
    [SerializeField]
    private QuizSystem quizSystem;

    [SerializeField]
    private Transform hostPlayerTransform;
    [SerializeField]
    private Transform clientPlayerTransform;
    [SerializeField]
    private Transform hostMachineTransform;
    [SerializeField]
    private Transform clientMachineTransform;
    [SerializeField]
    private Transform hostEffectTransform;
    [SerializeField]
    private Transform clientEffectTransform;
    public override void OnNetworkSpawn()
    {
        NetworkManager.Singleton.SceneManager.OnLoadEventCompleted += SceneLoaded;    
    }

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void SceneLoaded(string sceneName, LoadSceneMode loadSceneMode, List<ulong> clientsCompleted, List<ulong> clientsTimeOut)
    {
        if (IsHost && sceneName == "GamePlayScene_2") 
        {
            StartCoroutine(SpawnCharacters(clientsCompleted));
        }
    }

    IEnumerator SpawnCharacters(List<ulong> clientIds)
    {
        yield return new WaitForSeconds(1f); // Wait for a second to make sure the scene is fully loaded

        foreach (ulong id in clientIds)
        {
            Transform playerTransform;
            Transform machineTransform;
            Transform effectTransform;

            if (NetworkManager.ServerClientId == id)
            {
                playerTransform = hostPlayerTransform;
                machineTransform = hostMachineTransform;
                effectTransform = hostEffectTransform;
            }
            else
            {
                playerTransform = clientPlayerTransform;
                machineTransform = clientMachineTransform;
                effectTransform = clientEffectTransform;
            }

            GameObject machineObj = Instantiate(machine, machineTransform.position, machineTransform.rotation);
            machineObj.transform.localScale = machineTransform.localScale;
            machineObj.GetComponent<NetworkObject>().SpawnWithOwnership(id, true);

            GameObject effectObj = Instantiate(effect, effectTransform.position, effectTransform.rotation);
            effectObj.GetComponent<NetworkObject>().SpawnWithOwnership(id, true);

            GameObject playerObj = Instantiate(player, playerTransform.position, playerTransform.rotation);
            playerObj.GetComponent<NetworkObject>().SpawnAsPlayerObject(id, true);
        }
    }
}
