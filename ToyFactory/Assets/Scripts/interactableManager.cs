using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class interactableManager : MonoBehaviour
{
    private Toggle toggle;

    void Start()
    {
        toggle = FindObjectOfType<RelayLobby>().gameObject.transform.GetChild(3).GetChild(2).GetComponent<Toggle>();
    }

    public void onClick() {
        toggle.interactable = true;
    }
}
