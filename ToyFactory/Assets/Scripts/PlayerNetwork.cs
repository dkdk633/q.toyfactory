using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class PlayerNetwork : NetworkBehaviour
{
    [SerializeField] 
    private float moveSpeed = 10f;
    [SerializeField]
    private Transform characterBody;
    [SerializeField]
    private Transform cameraCenter;

    private Animator animator;

    private bool canMove = false;
    private bool stop = false;

    private CountDown countDown;
    private NetworkVariable<bool> canStart = new NetworkVariable<bool>(false, NetworkVariableReadPermission.Everyone);

    private QuizSystem quizSystem;

    public override void OnNetworkSpawn()
    {
        canStart.OnValueChanged += OnCanStartChanged;
        countDown = GameObject.FindObjectOfType<CountDown>();

        //conveyor, gauge bar ����
        if (IsOwner)
        {
            quizSystem = GameObject.Find("QuizCanvas").GetComponent<QuizSystem>();
            GameObject[] Machines = GameObject.FindGameObjectsWithTag("Machine");
            foreach (var machine in Machines)
            {
                NetworkObject networkObject = machine.GetComponent<NetworkObject>();
                Debug.Log("networkObject id: " + networkObject.OwnerClientId);
                Debug.Log("local id: " + NetworkManager.LocalClientId);
                if (networkObject.OwnerClientId == NetworkManager.LocalClientId)
                {
                    quizSystem.SetConveyor(machine.transform.GetChild(7).GetChild(0).gameObject);
                    quizSystem.SetGaugeBar(machine.transform.GetChild(8).GetChild(0).gameObject);
                    break;
                }
            }
        }

        if (IsHost)
        {
            if (NetworkManager.ConnectedClientsList.Count > 1)
            {
                return;
            }
            else
            {
                canStart.Value = true;
            }
        }

        if (!IsOwnedByServer)
        {
            SetCanStartServerRpc();
        }

        
    }

    [ServerRpc]
    void SetCanStartServerRpc()
    {
        canStart.Value = true;
    }

    private void OnCanStartChanged(bool oldValue, bool newValue)
    {
        if (newValue)
        {
            Debug.Log(countDown);
            countDown.SetStartCount(true);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        animator = characterBody.GetComponent<Animator>();   
    }

    // Update is called once per frame
    void Update()
    {
        if (!IsOwner)
            return;
        if (!canMove) 
        {
            return;
        }
        LookAround();
        Move();
    }

    private void LookAround()
    {
        Vector2 mouseDelta = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
        Vector3 camAngle = cameraCenter.rotation.eulerAngles;

        float x = camAngle.x - mouseDelta.y;

        if (x < 180f)
        {
            x = Mathf.Clamp(x, -1f, 70f);
        }
        else 
        {
            x = Mathf.Clamp(x, 335f, 361f);
        }
        cameraCenter.rotation = Quaternion.Euler(x, camAngle.y + mouseDelta.x, camAngle.z);
    }
   
    private void Move()
    {
        Vector2 moveInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        bool isMove = moveInput.magnitude != 0;
        animator.SetBool("isMove", isMove);
        if(isMove)
        {
            Vector3 lookForward = new Vector3(cameraCenter.forward.x, 0f, cameraCenter.forward.z).normalized;
            Vector3 lookRight = new Vector3(cameraCenter.right.x, 0f, cameraCenter.right.z).normalized;
            Vector3 moveDir = lookForward * moveInput.y + lookRight * moveInput.x;

            characterBody.forward = moveDir;
            transform.position += moveDir * Time.deltaTime * moveSpeed;
        }
    }

    private void Stop()
    {
        animator.SetBool("isMove", false);
    }

    public void SetCanMove(bool canMove)
    {
        this.canMove = canMove;
        if(!canMove)
            Stop();
    }

    public QuizSystem GetQuizSystem( )
    {
        return this.quizSystem;
    }
}
