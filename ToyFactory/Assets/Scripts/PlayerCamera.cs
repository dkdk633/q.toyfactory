using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class PlayerCamera : NetworkBehaviour
{
    [SerializeField]
    private GameObject mainCamera;
    private Vector3 cameraLocalPosition = new Vector3(0, 1.2f, -2.8f);
    private Vector3 cameraLocalRotation = new Vector3(12, 0, 0);
    public override void OnNetworkSpawn()
    {
        if (IsOwner) 
        {
            mainCamera = GameObject.Find("Main Camera");
            mainCamera.transform.parent = transform.GetChild(0);
            mainCamera.transform.localPosition = cameraLocalPosition;
            mainCamera.transform.localRotation = Quaternion.Euler(cameraLocalRotation);

        }
    }
}
