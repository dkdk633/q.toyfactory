using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;


public class QuizRanking : MonoBehaviour
{
    public GameObject quizTextPrefab; // Text 프리팹
    public Transform contentPanel; // Content Panel
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(GetQuizzes());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    IEnumerator GetQuizzes()
    {
        int i = 0;
        string url = $"http://localhost:1234/quiz-execution/ranking";
        UnityWebRequest www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.LogError("Failed to get quizzes: " + www.error);
        }
        else
        {
            QuizEntityList quizEntities = JsonUtility.FromJson<QuizEntityList>("{\"quizzes\":" + www.downloadHandler.text + "}");
            Debug.Log(quizEntities);

            // 받은 퀴즈 리스트를 UI에 표시
            foreach (var quiz in quizEntities.quizzes)
            {
                i++;
                GameObject newQuizText = Instantiate(quizTextPrefab, contentPanel);
                newQuizText.GetComponent<TMP_Text>().text = $" \n  {i} 등. 강의노트 : {quiz.lectureName}, 실행 횟수: {quiz.executionCount} \n";
            }
        }
    }
}
