﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OpenAI_API;
using OpenAI_API.Chat;
using OpenAI_API.Models;
using UnityEngine.UI;
using TMPro;
using System.IO;
using System.Threading.Tasks;


public class quizGenerator : MonoBehaviour
{

    private UploadQuiz _uploadquiz;
    private string inputLectureNoteStr;
    private OpenAIAPI openAIApi;
    [SerializeField] TMP_InputField quizNum;
    [SerializeField] TMP_Dropdown quizDropdown;
    private string gptKey, quizType;
    private string response;
    [SerializeField] Button quizListButton;
    [SerializeField] GameObject gameStartButton, quizWaitText, fileNameText;
    private bool init = false;

    private void Start()
    {
        _uploadquiz = FindObjectOfType<UploadQuiz>();
        FileInfo fileInfo = new FileInfo("./chatGPT_API_Key.txt");

        if (fileInfo.Exists)
        {
            StreamReader reader = new StreamReader("./chatGPT_API_Key.txt");
            gptKey = reader.ReadToEnd();
            reader.Close();
        }
        else
        {
            Debug.LogError("파일이 존재하지 않습니다");
        }

        openAIApi = new OpenAIAPI(gptKey);
    }

    public async void makeQuiz()
    {
        string quizResultStr = "";

        if (string.IsNullOrWhiteSpace(inputLectureNoteStr))
        {
            Debug.Log("강의노트를 선택해주세요.");
            return;
        }
        if (string.IsNullOrWhiteSpace(quizNum.text))
        {
            Debug.Log("생성될 문제의 개수를 입력해주세요.");
            return;
        }
        if(int.Parse(quizNum.text) <= 0)
        {
            return;
        }

        quizWaitText.SetActive(true);
        Debug.Log("퀴즈 개수: " + quizNum.text);
        Debug.Log("선택된 옵션" + quizDropdown.options[quizDropdown.value].text);

        gameStartButton.GetComponent<Toggle>().interactable = false;
        quizListButton.interactable = false;

        var chat = openAIApi.Chat.CreateConversation();
        chat.Model = Model.ChatGPTTurbo;
        chat.RequestParameters.Temperature = 0.7f; //값이 높을 수록 답이 창의적으로 나오지만 리스크가 큼

        chat.AppendSystemMessage("나는 대학교 강의노트의 내용을 퀴즈를 풀면서 공부하고 싶어. 너는 내가 제시한 지문을 바탕으로 입력한 개수만큼 퀴즈를 생성해주면 돼. 너의 대답에는 질문, 정답, 해설 이 3가지가 필수로 포함되어야 해. 유형은 5지선다형 문제, ox 문제가 있어. 문제는 Q. 뒤에 써주고 정답은 A. 뒤에 써주고 해설은 R. 앞에 써줘."); //gpt에서 시스템으로서의 역할과 무엇을 해야하는지 세부적으로 설명

        //유저랑 어시스턴트 대화 예시
        chat.AppendUserInput("치토는 아주대학교의 마스코트이고 기룡이는 경기대학교의 마스코트이다. 이 글을 바탕으로 문제를 1개 만들어줘. 문제의 유형을 5지선다형이야.");
        chat.AppendExampleChatbotOutput("Q. 아주대의 마스코트 캐릭터는 누구일까요?\n\n a) 치토\nb) 기룡이\nc) 넙죽이\nd) 한양이\ne) 눈송이\n\nA. a)\n\n 해설: 아주대의 마스코트는 치토입니다."
            /*"\n\nQ. 아주대의 개교기념일은 언제인가요?\n\n a) 4월 11일\nb) 4월 12일\nc) 4월 13일\nd) 4월 14일\ne) 4월 15일\n\nA. a)\n\n 해설: 아주대의 개교기념일은 4월 12일입니다."*/);
        //chat.AppendUserInput("제시된 글을 바탕으로 문제 2개를 만들어줘. 문제는 5지선다형 1개와 OX형 1개로 구성되어야 해.");
        //chat.AppendExampleChatbotOutput("Q. 아주대의 마스코트 캐릭터는 누구일까요?\n\n a) 치토\nb) 기룡이\nc) 넙죽이\nd) 한양이\ne) 눈송이\n\nA. a)\n\n 해설: 아주대의 마스코트는 치토입니다.\n\n" +
        //    "\n\nQ. 아주대 디지털미디어학과는 팔달관을 사용한다.\n\n A. X\n\n 해설: 아주대 디지털미디어학과는 산학원을 사용합니다.");
        //Q. 질문   , a) b) c) d) e) , A. b)

        await chat.GetResponseFromChatbotAsync();


        if (quizDropdown.value == 0)
        {
            GameSceneUserDataManager.Instance().SetQuizOption(5);
        }
        else if(quizDropdown.value == 1)
        {
            GameSceneUserDataManager.Instance().SetQuizOption(2);
        }
        //실제로 질문하는 부분
        if (quizDropdown.value != 2)
        {
            chat.AppendUserInput(inputLectureNoteStr + "\n 이 글을 바탕으로 퀴즈를 " + quizNum.text + "개 만들어줘. 문제는 한개씩 만들고 내가 'continue'를 입력하면 다음 문제를 만들어줘. 문제의 유형은 " + quizDropdown.options[quizDropdown.value].text + "이고 해설도 꼭 써줘 해설은 앞에 R. 로만 표시해줘 . 문제는 Q로만 표시하고 번호는 쓰지 말아줘.");
            response = await chat.GetResponseFromChatbotAsync();
            Debug.Log(response);
            quizResultStr += response + "\n\n";
            for (int i = 1; i < int.Parse(quizNum.text); i++)
            {
                if (i % 5 == 0)
                {
                    await Task.Delay(10000);
                    chat.AppendUserInput("아까 글을 바탕으로 퀴즈를 마저 만들어줘. 문제는 한개씩 만들고 내가 'continue'를 입력하면 다음 문제를 만들어줘. 문제의 유형은 " + quizDropdown.options[quizDropdown.value].text + "이고 해설도 꼭 써줘. 문제는 Q로만 표시하고 번호는 쓰지 말아줘.");
                    response = await chat.GetResponseFromChatbotAsync();
                    Debug.Log(response);
                    quizResultStr += response + "\n\n";
                }
                else
                {
                    chat.AppendUserInput("continue");
                    response = await chat.GetResponseFromChatbotAsync();
                    Debug.Log(response);
                    quizResultStr += response + "\n\n";
                }

            }
        }
        else
        {
            chat.AppendUserInput(inputLectureNoteStr + " 이 글을 바탕으로 먼저 답이 1개인 5지선다형 문제 " + (int.Parse(quizNum.text)) / 2 + "개를 만들어줘. 문제는 한개씩 만들고 내가 'continue'를 입력하면 다음 문제를 만들어줘. 해설도 꼭 써줘 해설은 앞에 R. 을 써줘 . 문제는 Q로만 표시하고 번호는 쓰지 말아줘.");
            response = await chat.GetResponseFromChatbotAsync();
            Debug.Log(response);
            quizResultStr += response + "\n\n";

            // 퀴즈 옵션 설정 -> db 에게 알리기
          
            
            // 대화 
            for (int i = 1; i < int.Parse(quizNum.text) / 2; i++)
            {
                if (i % 5 == 0)
                {
                    await Task.Delay(10000);
                    chat.AppendUserInput("아까 글을 바탕으로 퀴즈를 마저 만들어줘. 문제는 한개씩 만들고 내가 'continue'를 입력하면 다음 문제를 만들어줘.");
                    response = await chat.GetResponseFromChatbotAsync();
                    Debug.Log(response);
                    quizResultStr += response + "\n\n";
                }
                else
                {
                    chat.AppendUserInput("continue");
                    response = await chat.GetResponseFromChatbotAsync();
                    Debug.Log(response);
                    quizResultStr += response + "\n\n";
                }
            }
            
            
            // 퀴즈 옵션 설정 -> db 에게 알리기
          
            
            // 대화 
            chat.AppendUserInput("아까 글을 바탕으로 OX형 문제 " + (int.Parse(quizNum.text) - (int.Parse(quizNum.text)) / 2) + "개를 만들어줘. 문제는 한개씩 만들고 내가 'continue'를 입력하면 다음 문제를 만들어줘. 해설도 꼭 써줘 해설은 앞에 R. 을 써줘. 문제는 Q로만 표시하고 번호는 쓰지 말아줘.");
            response = await chat.GetResponseFromChatbotAsync();
            Debug.Log(response);
            quizResultStr += response + "\n\n";
            for (int i = 1; i < (int.Parse(quizNum.text) - (int.Parse(quizNum.text)) / 2); i++)
            {
                if (i % 5 == 0)
                {
                    await Task.Delay(10000);
                    chat.AppendUserInput("아까 글을 바탕으로 퀴즈를 마저 만들어줘. 문제는 한개씩 만들고 내가 'continue'를 입력하면 다음 문제를 만들어줘.");
                    response = await chat.GetResponseFromChatbotAsync();
                    Debug.Log(response);
                    quizResultStr += response + "\n\n";
                }
                else
                {
                    chat.AppendUserInput("continue");
                    response = await chat.GetResponseFromChatbotAsync();
                    Debug.Log(response);
                    quizResultStr += response + "\n\n";
                }
            }
            
        }

        //response = await chat.GetResponseFromChatbotAsync();
        //Debug.Log(response);
        quizNum.text = "";
        inputLectureNoteStr = "";
        quizDropdown.value = 0;
        fileNameText.GetComponent<TextMeshProUGUI>().text = "파일 이름";

        //1. 싱글톤 클래스에 저장
        Debug.Log(quizResultStr);
        GameSceneUserDataManager.Instance().SetQuizString(quizResultStr);
        
        //2. db 에 업로드
        _uploadquiz.Report();

        _uploadquiz.setQuizStr(quizResultStr);

        //foreach (ChatMessage msg in chat.Messages)
        //{
        //    Debug.Log($"{msg.Role}: {msg.TextContent}");
        //}
        //Debug.Log(chat.Messages.Count);

        quizWaitText.SetActive(false);
        quizListButton.interactable = true;
        gameStartButton.GetComponent<Toggle>().interactable = true;
    }

    public string GetQuizText()
    {
        return response;
    }

    public void setConvertedQuizText(string text)
    {
        inputLectureNoteStr = text;
    }
}
